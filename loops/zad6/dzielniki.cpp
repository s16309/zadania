#include <iostream>
#include <conio.h>

using namespace std;

int main(){
	int n;

	cout << "podaj n: " << endl;
	cin >> n;

	cout << "for version"<< endl;

	for(int i = 1; i<=n; i++){
		if(n%i == 0){
			cout<< i << " jest dzielnikiem " << n << endl;
		}
	}

	_getch();

	cout << "while version"<< endl;

	int j = 1;
	while(true){
		if(n%j == 0){
			cout<< j << " jest dzielnikiem " << n << endl;
		}

		if(j==n){
			break;
		}

		j++;
	}

	_getch();

	return EXIT_SUCCESS; 
}